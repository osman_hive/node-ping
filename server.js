const express = require("express");
var mongoose = require('mongoose');
// const moment = require('moment');
var zone_moment = require('moment-timezone');
const app = express();
const port = 3000;

app.get("/api/user/pingbroadcast", async (req, res) => {
    try {
        let userIdStr = req.query.user_id;
        let userTypeStr = req.query.type;
        if(!userIdStr || !userTypeStr) {
            res.status(400).json({
                "status"    : false,
                "error"     : "Userid or Usertype not specified.",
            })
        } 

        var mongoDB = 'mongodb://localhost:27017/data_live_new';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error:'));

        var broadcastStatus = mongoose.model('broadcast_statuses', new mongoose.Schema({
            user_id                 : Number,
            user_type               : Number,
            status                  : Number,
            host_last_updated_at    : Date
        }));

        var userIdInt = parseInt(userIdStr, 10);
        var userTypeInt = parseInt(userTypeStr, 10);        
        
        // var time = zone_moment.tz('Asia/Dhaka').format("YYYY-MM-DDTHH:MM:ss");
        // var dateTime = new Date(time);
        var message = "";
        var bStatus = await broadcastStatus.findOne({user_id: userIdInt});
        if(bStatus) {
            if(userTypeInt == 1) {
                //update host time
            }
            bStatus = await broadcastStatus.findOneAndUpdate(
                { "user_id"    : userIdInt }, 
                { "$set" : {
                        "user_type"              : userTypeInt,
                        "status"                 : 0//,
                        // "host_last_updated_at"   : dateTime
                    }
                },
                { new    : true }           
            );            
            if(userTypeInt == 0 && bStatus.status == 2) {
                message = "Your Account status Disable. Please conact With Hive Admin.";
            }
        } else {
            bStatus = new broadcastStatus({
                user_id         : userIdInt,
                user_type       : userTypeInt,
                status          : 0,
                host_last_updated_at    : new Date()
            });
            await bStatus.save();
        }

        res.status(200).json({
            "status":   true,
            "type": bStatus.user_type,
            "action_status" : bStatus.status,
            "message" : message
        });
    } catch (err) {
        res.status(500).json({
            status  : false,
            error   : err
        });
    }    
});

app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});